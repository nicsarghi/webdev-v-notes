---
layout: post
title:  "How Browsers Work"
date:   2022-08-27
---

# How Browsers Work
https://www.youtube.com/watch?v=uE3UPEK26U0&t=36s

## About Browsers
Browsers are complex, and drastically changed since the 90s
Went from rendering text to executing complex and fully fledged applications

---

## Protocols
Requesting documents on the internet, as such they can handle network connections

### Then
- Used HTTP protocol to do CRUD operations on documents
- CRUD -> CREATE READ UPDATE DESTROY

### Now
- Bidirectional comm. channels with a server (WebSockets for example)
- PTP connections with another browser

## Rendering / Executing
- The browsers themselves need to know how to parse the documents being sent to them so that they can render the document (HTML, CSS, SVG) or execute code (JS)
- Also need to render fonts, images, audio and video.

## Interactivity
- Selecting text,
- Scroll pages 
- Clicking on links 
- Inputting text
- Drag and drop

## Storing Data
- Caching data (Service Workers + Cache API)
- Providing storing mechanisms (LocalStorage, SessionStorage, IndexedDB)

**And they all have to make this functional no matter how complex the page, and no matter how many pages are open. Apparently really hard to achieve.**

## Security
- Internet is the Wild West; content can be from anywhere and any source.
- Everything has to be pretty secured, vulnerabilities can compromise a regular user at any time
- Which means they need to manage data encryption (TLS/SSL and WebCrypto)
- Sandbox content so that malicious code doesnt access other content

**Plus numerous other APIs that you don't even know about**

--- 

## How Browsers Render Documents
- Browser requests all documents (HTML and links inside of it like stylesheets or images)
- Documents are parsed and turned into machine-friendly representations (DOM trees/Style trees/Rendering trees )
- Intermediate representations are used to compute the entire page layout (MIXING ALL THINGS). Computes size and position of everything. Also the most complicated part of the browser
- The browser paints each pixel onto the screen so the viewer can see it

---

## Interactivity
Technically any piece of software displaying content does the above as well, but the w.eb is more interactive. It's dynamic, which means that the full layout is recomputed at 30-60 fps (EVEN IF YOU HAVE A LOT ON YOUR SCREEN.) Triple-A game developers also face this type of challenge interestingly enough.

### Basic interactivity
- Done through html and css
- Select pages and clicking on links. 

### JavaScript
- Lets you make any type of interactivity since you have access to the functionalities of the browsers.

## User experience and Interactivity
- Web developers need to know how browsers work because of optimization.  
**Ex:** Figuring out what triggers layout recomputations vs repainting (second is faster)
- Still kind of hard because each browser works differently (somewhat)

